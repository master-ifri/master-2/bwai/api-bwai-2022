var aws = require('aws-sdk');
var multer = require('multer');
var multerS3 = require('multer-s3');


//********  Save file on local 

const s3 = new aws.S3({
  region: process.env.S3_REGION,
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_ACCESS_SECRET
});

function checkDocFileType(file, cb){
  // Allowed ext
  const filetypes = /pdf|docx|ppt|pptx|doc/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Images Only!');
  }
}

// const DocfileFilter = (req, file, cb) => {
//   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//     cb(null, true);
//   } else {
//     cb(new Error("Invalid file type, only JPEG and PNG is allowed!"), false);
//   }
// };

exports.imgFileOnS3 = multer({
  // checkDocFileType,
  storage: multerS3({
    s3: s3,
    bucket: process.env.S3_BUCKET,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, file.originalname.toString())
    }
  })
});

exports.docFileOnS3 = multer({
  // checkDocFileType,
  storage: multerS3({
    s3: s3,
    bucket: process.env.S3_BUCKET,
    metadata: function (req, file, cb) {
      cb(null, {fieldName: file.fieldname});
    },
    key: function (req, file, cb) {
      cb(null, file.originalname.toString())
    }
  })
});


//*********  Save file on local serveur

const storage = multer.diskStorage({
  destination: './uploads/',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

// Init Upload
exports.uploadfile = multer({
  storage: storage,
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  },
  dest: './uploads/',
  preservePath: true,
});

exports.saveMyFile = multer({
  storage: multer.diskStorage({
    destination: function(req, file, cb){
     cb(null, './uploads/');
    },
    filename: function(req, file, cb){
      // console.log("Our file ", file);
     cb(null, new Date().toISOString() + '.png');
    },
    fileFilter: function(req, file, cb){
      checkFileType(file, cb);
    },
  })
});

// Check File Type
function checkFileType(file, cb){
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Images Only!');
  }
}
