let User = require('./UserModel')
//let bcrypt = require('bcrypt')

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

  passport.use(new LocalStrategy(
    
  function(username, password, done) {
    User.findOne({ username: username }, (err, user) =>{
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      //await bcrypt.compare(password,user.passe)
      if (user.password != password) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

/*
const LocalStrategy = require('passport-local').Strategy

module.exports.fonction = function initialize(passport){

    const authenticateUser = async (mail,passe,done)=>{
        User.findOne({mail:mail},(err,theuser)=>{
            if(err){
                resp.send(err);
            }
            const user = theuser
        });        
        if(user == null){ return DocumentTimeline(null,false,{message:'Pas d utilsateur ayant ce mail.'})}

        try {
            if( passe,user.passe){
                return done(null,user)
            }else{ return done(null,false,{message:'Mot de passe incorrect.'})}
        } catch (error) { return done(error) }
    }

    passport.use(new LocalStrategy({usernameField:'mail'},
    authenticateUser))
    passport.serializeUser((user,done)=>{})
    passport.deserializeUser((id,done)=>{})
}
*/