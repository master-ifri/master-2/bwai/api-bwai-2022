let mongoose = require('mongoose') 

let UserSchema = mongoose.Schema({
  email: {type: String, unique: true},
  password: {type: String},
  firstname: {type: String},
  lastname: {type: String},
  role: { 
    type: String, 
    enum: ['reviewer','admin'], 
    default: 'reviewer'
  },
  statut: { type : Boolean, default: true },
  contact: {type: String},

},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

module.exports = mongoose.model('User', UserSchema);