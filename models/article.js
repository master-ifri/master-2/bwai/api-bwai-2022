const mongoose = require('mongoose') 

const ArticleSchema = mongoose.Schema({
  email: {type: String, require: true},
  doc: {type: String, require: true},
  docname: {type: String},
  format: {type: String},
  etat: { 
    type: String, 
    enum: ['assign','unassign'], 
    default: 'unassign'
  },
},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

module.exports = mongoose.model('Article', ArticleSchema);