const mongoose = require('mongoose') 

const ReviewSchema = mongoose.Schema({
  commentaire: {type: String, default: ''},
  deadline: {type: String},
  delivered: { type : Boolean, default: false },
  decision: { 
    type: String, 
    enum: ['neutre','rejete','accepte','update'], 
    default: 'neutre'
  },
  article: {type: mongoose.Schema.Types.ObjectId, ref: 'Article' },
  reviewer: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  admin: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },

},
{
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

module.exports = mongoose.model('Review', ReviewSchema);