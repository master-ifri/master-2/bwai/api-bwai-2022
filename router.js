let express = require('express');

let authController = require('./controllers/auth');
let userController = require('./controllers/user');

let articleController = require('./controllers/article');
let reviewController = require('./controllers/review');

// let mainController = require('./controllers/main');

// let historiqueController = require('./controllers/historique');


let s3 = require('./services/s3');

const isLogged = authController.verifyLoggedUser;

module.exports = (function() {
  let router = express.Router();


  // Users
  router.post('/users/login', authController.userLogin);
  router.get('/users', isLogged, userController.loadMe);
  router.post('/users/register', userController.register);
  // router.post('/users/registerLogin', userController.registerWithLogin);
  router.get('/users/list',isLogged, userController.list);
  router.get('/users/:id',isLogged, userController.getDetails);
  router.get('/users-count',isLogged, userController.count);

  // router.get('/users/confirm-account/:id', userController.confirmeAccount);
  router.post('/users/forget-password', userController.forgetPassword);
  router.put('/users/:id/reset-password', userController.resetPassword);

  router.put('/users/update/:id', isLogged, userController.update);
  router.delete('/users/remove/:id', isLogged, userController.delete);



  // Articles
  router.get('/articles/list',isLogged, articleController.list);
  router.get('/articles/:id',isLogged, articleController.getDetails);
  router.get('/articles-count',isLogged, articleController.count);

  router.post('/articles/createWithFile', s3.docFileOnS3.single('doc'), articleController.createWithFile);
  router.put('/articles/update/:id', isLogged, s3.docFileOnS3.single('doc'), articleController.update);
  
  router.delete('/articles/remove/:id',isLogged, articleController.delete);

  
  // Reviewes
  router.get('/reviews/list',isLogged, reviewController.list);
  router.get('/reviews/:id',isLogged, reviewController.getDetails);
  router.get('/reviews-count',isLogged, reviewController.count);
  router.post('/reviews/create',isLogged, reviewController.create);
  router.put('/reviews/update/:id',isLogged, reviewController.update);
  router.delete('/reviews/remove/:id',isLogged, reviewController.delete);


  // Main

  // router.get('/main-count',isLogged, mainController.count);

  // // Historique
  // router.get('/historiques/list',isLogged, historiqueController.list);
  // router.get('/historiques-count',isLogged, historiqueController.count);
  // router.delete('/historiques/remove/:id',isLogged, historiqueController.delete);

  return router;
})(); 
