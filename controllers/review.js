const { options } = require('joi');
const Review = require('../models/review');
const Article = require('../models/article');
// let History = require('../models/historiques');

const nodemailer = require('nodemailer');

const mailParams = {
  // host: 'smtp.mail.us-east-1.awsapps.com',
  host : process.env.mailHost,
  // port: 587,
  port: process.env.mailPORT,
  secure: true,
  auth: {
    user: process.env.mailUser,
    pass: process.env.mailPassword
  },
  tls: {
    rejectUnauthorized: false
  }
};

exports.create = async function (req, res) {
  const article = req.body.article;
  const deadline = req.body.deadline; 
  const delivered = req.body.delivered; 
  const commentaire = req.body.commentaire;  
  const decision = req.body.decision; 
  const reviewer = req.body.reviewer;
  const admin = req.body.admin;

  try {
    const new_Review = new Review({
      commentaire,
      article,
      decision,
      deadline,
      delivered,
      reviewer,
      admin
    });
      
    const lastData = await new_Review.save();
    console.log("Req body ", req.body);

    if(lastData){

      // Go to update article etat
      let currentArticle = await Article.findOne({_id: req.body.article}).select();

      if(currentArticle){
        currentArticle.etat = 'assign';
        const updateArticle = await currentArticle.save();

        if(updateArticle){
          const resSendEmail = await newReviewAssign(req.body.email);
          
          if(resSendEmail==true){
            console.log("Email was sended to reviewer");
          }
          console.log("Article is up to date");

        } else {
          console.log("Article isn't up to date");
        }

      } else {
        console.log("Article not found");
      }

      return res.json({
        status: 200,
        message: "Article assigné"
      });

    } else {
      return res.json({
        status: 404,
        message: "Une erreur s'est produite. Veuillez réessayer!"
      });
    }

  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.getDetails = async function(req, res){
  const resourceId = req.params.id;

  try {
    let resources = await Review.findOne({_id: resourceId}).select();
    
    res.json({
      status: 200,
      ressources: resources
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.count = async function(req, res) {
  try {
    let query = {};
    let total = await Review.countDocuments();
    
    res.json({
      status: 200,
      ressources: total
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.list = async function (req, res) {
  // const resId = req.params.id || '';
  try {

    // const query = req.query.options ? JSON.parse(req.query.options) : {};
    let option = req.query.options ? JSON.parse(req.query.options) : {};
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const sort = req.query.sort ? req.query.sort : {"createdAt": -1};
    const perPage = req.query.perPage ? parseInt(req.query.perPage) : 50;

    let options = {};
    if(req.query.filter && JSON.parse(req.query.filter).text && JSON.parse(req.query.filter).text.length>1){
      options = {
        ...option,
        $or:[
          // {article: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          // {password: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {commentaire: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {delivered: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {decision: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
        ]
      };
    }

    const resources = await Review.find(options)
      .populate(['admin','article','reviewer'])
      .skip(page * perPage)
      .limit(perPage)
      .sort({"createdAt": -1})
      .lean();

    const countDatabase = await Review.find(options).countDocuments();

    res.json({
      status: 200,
      ressources: resources,
      total: resources.length,
      count: countDatabase
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function (req, res) {
  const id = req.params.id;
  try {
    let foundReview =  await Review.findOne({_id: id});

    foundReview.commentaire = req.body.commentaire || foundReview.commentaire;
    foundReview.decision = req.body.decision || foundReview.decision;

    foundReview.deadline = req.body.deadline || foundReview.deadline;
    foundReview.delivered = req.body.delivered || foundReview.delivered;
    
    foundReview.article = req.body.article || foundReview.article;
    foundReview.reviewer = req.body.reviewer || foundReview.reviewer;
    foundReview.admin = req.body.admin || foundReview.admin;


    const savedData = await foundReview.save();

    // const newHistory = new History({
    //   action: "Mises à jour d'une Review sur la plateforme",
    //   createdAt: savedData.updatedAt,
    //   user: req.body.userId
    // });

    // await newHistory.save();
    
    res.json({
      status: 200,
      ressources: savedData,
      message: 'Review is up to date successfully'
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};


exports.delete = async function (req, res) {
  const id = req.params.id;
  try {
    await Review.findByIdAndRemove(id);

    // const newHistory = new History({
    //   action: "Suppression d'une Review sur la plateforme",
    //   user: req.body.userId
    // });

    // await newHistory.save();

    res.json({
      status: 200,
      ressources: savedData,
      message: 'the Review was deleted.'
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }  
};

const newReviewAssign = async function(mail) {
  try {
    let resp = {};

    let transporter = nodemailer.createTransport(mailParams);

    let mailOptions = {
      from: mailParams.auth.user,
      // to: request.payload.email,
      // to: 'ronald.hounkponou@gmail.com',
      // to: 'bchryzal@gmail.com',
      to: mail,
      subject: "Nouvelle activité sur BWAI",
      // text: 'Je vous contacte...',
      // html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>'+ request.payload.message +' <br>',
      html: `
      <!DOCTYPE html>
      <html lang="fr">
        <body style="padding: 10%; background-color: #eee; ">
          <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
            
      
            <hr style="color: #78b843; background-color: #78b843;">
            <h4>Bonjour,</h4>
            <p>
              Un nouvel article vient de vous être assigné pour un review.
            </p>
            <p>
              Veuillez cliquer sur le boutton ci-dessous pour consulter l'activité.
              <br> <br>
              <div style=" text-align: center;">
                <a href="https://bwai.herokuapp.com/auth" target="_blank" style="padding: 12px 8px;
                  background-color: #78b843; color: white; border-radius: 5px; ">
                  Consulter l'activité
                </a>
              </div>
      
              <br><br><br>
              Si vous n'avez pas créé un compte sur BWAI, veuillez ignorer ce mail.
              <br><br>
              Coordialement
              <br><br>
              BWAI
              
            </p>
            <hr>
            <p>
              bwai.com - © Tout droit réservé 2022
       
            </p>
          </div>
        </body>
      </html>
      `,
    };

    transporter.sendMail(mailOptions, (err, info) => {
      if(err){
        console.log('Error transporter sendmail :', err);
        return false;
      }
      console.log('Message sent infos :', info);
      resp = info.messageId;
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};