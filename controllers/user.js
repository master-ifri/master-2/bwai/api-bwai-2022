let User = require('../models/user');
// let Customer = require('../models/customer');
const authController = require('./auth');
const uuidv4 = require('uuid');
//let History = require('../models/historiques');
const nodemailer = require('nodemailer');

const mailParams = {
  // host: 'smtp.mail.us-east-1.awsapps.com',
  host : process.env.mailHost,
  // port: 587,
  port: process.env.mailPORT,
  secure: true,
  auth: {
    user: process.env.mailUser,
    pass: process.env.mailPassword
  },
  tls: {
    rejectUnauthorized: false
  }
};

// const uuidv4 = v4;

exports.registerWithLogin = async function (req, res) {
  const email = req.body.email;  
  const password = req.body.password;  
  const firstname = req.body.firstname; 
  const lastname = req.body.lastname;  
  const role = req.body.role;
  const contact = req.body.contact;
  const newsletters = req.body.newsletters || false;

  try {
    let oldUser = await User.findOne({email});

    if (oldUser) {
      return res.json({
        status: 409,
        message: "L'email existe déjà !"
      });
    }

    let new_user = new User({
      email,
      password: authController.hashPassword(password),
      firstname,
      lastname,
      role,
      contact,
      newsletters
    });

    let savedUser = await new_user.save();

    // const customer = new Customer({
    //   nom: savedUser.firstname,
    //   prenom: savedUser.lastname,
    //   email: savedUser.email,
    //   password: '',
    //   statut: 'En attente',
    //   userId: savedUser._id
    // });

    // await customer.save();

    authController.login(new_user, res);
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.register = async function (req, res) {
  const email = req.body.email;  
  const password = req.body.password;  
  const firstname = req.body.firstname; 
  const lastname = req.body.lastname;  
  const role = req.body.role;
  const contact = req.body.contact;
  const statut = req.body.statut || false;
  

  try {
    const oldUser = await User.findOne({email});

    if (oldUser) {

      return res.json({
        status: 409,
        message: "L'email existe déjà !"
      });
      
    } else {

      let new_user = new User({
        email,
        password: authController.hashPassword(password),
        firstname,
        lastname,
        role,
        contact,
        statut
      });

      let savedUser = await new_user.save();
      //let confirmEmail = null;
      //console.log("Cool ", savedUser);

      if(savedUser){
        //confirmEmail = await sendConfirmEmail(savedUser._id, savedUser.email);
        //const notifyAllAdmin = await notifyAdmins();
        //console.log("Tout les admin ont été notifiés : ", notifyAllAdmin);
        
        res.json({
          status: 200,
          message: "Utilisateur enrégistré",
          user: savedUser,
          // sendmail: confirmEmail
        });
      }
      
    }
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.loadMe = async function (req, res) {
  if (req.user) {
    res.json({
      user: req.user
    });
  } else {
    res.status(400).send('User not found'); 
  }
};

exports.confirmeAccount = async function (req, res) {
  // const id = req.params.id;

  try {
    let foundUser = await User.findOne({_id: req.params.id});

    if(foundUser){

      if(foundUser.statut && foundUser.statut == true){

        res.json({
          status: 409,
          message: "Votre compte a déjà été confirmé! Vous pouvez vous connecter"
        });

      } else {
        
        foundUser.statut = true;
        const saveData = await foundUser.save();
  
        // const newHistory = new History({
        //   action: "Confirmation de compte sur la plateforme",
        //   user: foundUser._id,
        //   createdAt: saveData.updatedAt
        // });
    
        // await newHistory.save();
  
        res.json({
          status: 200,
          message: "Confirmation effectuée !"
        });
      }

    } else {

      res.json({
        status: 404,
        message: "Votre compte est introuvable! Vérifiez bien le lien de confirmation dans votre boite mail."
      });

    }

    
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.getDetails = async function(req, res){
  const resourceId = req.params.id;

  try {
    let resources = await User.findOne({_id: resourceId}).select('-password -__v');
    
    res.json({
      status: 200,
      resource: resources
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.count = async function(req, res) {
  try {
    let query = {};
    let total = await User.countDocuments();
    
    res.json({
      status: 200,
      resource: total
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};


exports.list = async function (req, res) {
  try {

    let query = req.query.options ? JSON.parse(req.query.options) : {};
    let page = req.query.page ? parseInt(req.query.page) : 0;
    let sort = req.query.sort ? req.query.sort : {"createdAt": -1};
    let perPage = req.query.perPage ? parseInt(req.query.perPage) : 50;

    let options = {};
    if(req.query.filter && JSON.parse(req.query.filter).text && JSON.parse(req.query.filter).text.length>1){
      options = { 
          $or:[
            {firstname: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
            {lastname: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
            {email: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
            {role: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          ]
        //   firstname: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") },
        //  lastname: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") } 
      };

    }

    const users = await User.find(options).select('-password -__v')
    .skip(page * perPage)
    .limit(perPage)
    .sort({"createdAt": -1})
    .lean();

    let database = await User.find(options).countDocuments() ;

    res.json({
      status: 200,
      count: database,
      users: users,
      total: users.length
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function (req, res) {
  const id = req.params.id;

  try {
    let foundUser =  await User.findOne({_id: id});

    foundUser.email = req.body.email || foundUser.email;
    foundUser.firstname = req.body.firstname || foundUser.firstname;
    foundUser.lastname = req.body.lastname || foundUser.lastname;
    foundUser.role = req.body.role || foundUser.role;
    foundUser.contact = req.body.contact || foundUser.contact;
    foundUser.statut = req.body.statut || foundUser.statut;
    
    if(req.body.password && req.body.password.length >7){
      foundUser.password = authController.hashPassword(req.body.password);
      console.log("=== Mot de passe mise à jour");
    }
  
    const saveData = await foundUser.save();
    saveData.password = '';

    // const newHistory = new History({
    //   action: "Mises à jour des informations d'un utilisateur de la plateforme",
    //   user: foundUser._id,
    //   createdAt: saveData.updatedAt
    // });

    // await newHistory.save();
    
    res.json({
      user: saveData,
      status: 200
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.forgetPassword = async function (req, res) {
  try {
    let newUser = await User.findOne({email: req.body.email}).select('-password -__v');
    
    // newUser.resetToken = uuidv4(); 
    // await newUser.save();
    console.log("User data ", newUser);

    if(newUser){
      let confirmEmail = await sendResetPassword(newUser._id, newUser.email);

      // console.log("dddddd => ", confirmEmail);
      res.json({
        status: 200,
        message: "Email envoyé",
        sendmail: confirmEmail
      });
    } else {
      res.json({
        status: 404,
        message: "Compte introuvable",
      });
    }
   
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }

};

exports.resetPassword = async function (req, res) {
  const id = req.params.id;

  try {
    let foundUser = await User.findOne({_id: id});
    console.log("user data ", foundUser);

    if(!foundUser){
      res.json({
        status: 404,
        message: "Compte introuvable!"
      });
    } else {

      if(req.body.password){
        
        foundUser.password = authController.hashPassword(req.body.password);
  
        await foundUser.save();
        
        res.json({
          status: 200,
          message: 'Mot de passe réinitialiser'
          // user: updatedUser,
        });

      } else {
        res.json({
          status: 409,
          message: 'Mot de passe introuvable'
        });   
      }        
      
    }

  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function (req, res) {
  const id = req.params.id;
  try {
    await User.findByIdAndRemove(id);
    res.send("the user was deleted.");
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }  
};

const sendConfirmEmail = async function(id, mail) {
  try {
    let resp = {};

    let transporter = nodemailer.createTransport(mailParams);

    let mailOptions = {
      from: mailParams.auth.user,
      // to: request.payload.email,
      // to: 'ronald.hounkponou@gmail.com',
      // to: 'bchryzal@gmail.com',
      to: mail,
      subject: "Confirmation d'adresse email",
      // text: 'Je vous contacte...',
      // html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>'+ request.payload.message +' <br>',
      html: `
      <!DOCTYPE html>
      <html lang="fr">
        <body style="padding: 10%; background-color: #eee; ">
          <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
            <img style="width: 40%; height: auto;"
              src="https://beaudcollection.s3.af-south-1.amazonaws.com/logo.svg" alt="logo CVAD">
      
            <hr style="color: #78b843; background-color: #78b843;">
            <h4>Bonjour,</h4>
            <p>
              Veuillez cliquer sur le boutton ci-dessous pour confirmer votre adresse email.
              <br> <br>
              <div style=" text-align: center;">
                <a href="https://bwai.herokuapp.com/email-verify/`+id+`/account" target="_blank" style="padding: 12px 8px;
                  background-color: #78b843; color: white; border-radius: 5px; ">
                  Vérifiez mon adresse email
                </a>
              </div>
      
              <br><br><br>
              Si vous n'avez pas créé un compte sur CVAD, veuillez ignorer ce mail.
              <br><br>
              Coordialement
              <br><br>
              CVAD
              
            </p>
            <hr>
            <p>
              bwai.com - © Tout droit réservé 2022
       
            </p>
          </div>
        </body>
      </html>
      `,
    };

    transporter.sendMail(mailOptions, (err, info) => {
      if(err){
        console.log('Error transporter sendmail :', err);
        return false;
      }
      console.log('Message sent infos :', info);
      resp = info.messageId;
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};

const sendResetPassword = async function(id, mail) {
  try {
    let resp = {};

    let transporter = nodemailer.createTransport(mailParams);

    let mailOptions = {
      from: mailParams.auth.user,
      // to: request.payload.email,
      // to: 'ronald.hounkponou@gmail.com',
      // to: 'bchryzal@gmail.com',
      to: mail,
      subject: "Réinitialisations de mot de passe",
      // text: 'Je vous contacte...',
      // html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>'+ request.payload.message +' <br>',
      html: `
      <!DOCTYPE html>
      <html lang="fr">
        <body style="padding: 10%; background-color: #eee; ">
          <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
            <img style="width: 40%; height: auto;"
              src="https://beaudcollection.s3.af-south-1.amazonaws.com/logo.svg" alt="logo CVAD">
      
            <hr style="color: #78b843; background-color: #78b843;">
            <h4>Bonjour,</h4>
            <p>
              Veuillez cliquer sur le boutton ci-dessous pour réinitialiser votre mot de passe.
              <br> <br>
              <div style=" text-align: center;">
                <a href="https://bwai.herokuapp.com/email-verify/`+id+`/reset-password" target="_blank" style="padding: 12px 8px;
                  background-color: #78b843; color: white; border-radius: 5px; ">
                  Réinitialiser mon mot de passe
                </a>
              </div>
      
              <br><br><br>
              Si vous n'avez pas demandez à récupérer le mot de passe de votre compte sur CVAD, veuillez ignorer ce mail.
              <br><br>
              Coordialement
              <br><br>
              CVAD
              
            </p>
            <hr>
            <p>
              bwai.com - © Tout droit réservé 2022
            </p>
          </div>
        </body>
      </html>
      `,
    };

    transporter.sendMail(mailOptions, (err, info) => {
      if(err){
        console.log('Error transporter sendmail :', err);
        return false;
      }
      console.log('Message sent infos :', info);
      resp = info.messageId;
    });

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};

const notifyAdmins = async function() {
  try {
    let resp = {};
    const options = { 
      role : "admin",
      statut : true,
    };

    const users = await User.find(options).select('-password -__v -firstname -lastname -contact -_id').lean();
    console.log("Users list :", users);

    if(users && users.length > 0){
      let mailInTxt = "";
      let isTheLast = false;
  
      for(i=0; i < users.length; i++){
        mailInTxt += ""+users[i].email;
        if(i != (users.length-1)){
          console.log(i);
          mailInTxt +=", "
        } else {
          isTheLast = true;
        }
      }
      console.log("Rendu mails txt ", mailInTxt);

      if(mailInTxt && mailInTxt != "" && isTheLast==true){
        const transporter = nodemailer.createTransport(mailParams);
    
        const mailOptions = {
          from: mailParams.auth.user,
          // to: request.payload.email,
          // to: 'ronald.hounkponou@gmail.com',
          // to: 'bchryzal@gmail.com',
          to: mailInTxt,
          subject: "Nouvelle inscription sur le portail CVAD",
          // text: 'Je vous contacte...',
          // html: '<h2> <b>' + request.payload.name + '</b></h2> <br><br>'+ request.payload.message +' <br>',
          html: `
            <!DOCTYPE html>
            <html lang="fr">
              <body style="padding: 10%; background-color: #eee; ">
                <div style="background-color: white; padding: 5px; border-color: black; border-width: 2px;">
                  <img style="width: 40%; height: auto;"
                    src="https://beaudcollection.s3.af-south-1.amazonaws.com/logo.svg" alt="logo CVAD">
    
                  <hr style="color: #78b843; background-color: #78b843;">
                  <h4>Bonjour,</h4>
                  <p>
                    Un utilisateur vient de s'inscrire sur le portail CVAD.
                    <br> <br>
                    <div style=" text-align: center;">
                      <a href="https://cvadbenin.org/connexion" target="_blank" style="padding: 12px 8px;
                        background-color: #78b843; color: white; border-radius: 5px; ">
                        Consulter l'activité
                      </a>
                    </div>
    
                    <br><br><br>
                    Si vous n'êtes pas un administrateur du portail CVAD, veuillez ignorer ce mail.
                    <br><br>
                    Coordialement
                    <br><br>
                    CVAD
                  </p>
                  <hr>
                  <p>
                    bwai.com - © Tout droit réservé 2022
                  </p>
                </div>
              </body>
            </html>
          `,
        };
    
        transporter.sendMail(mailOptions, (err, info) => {
          if(err){
            console.log('Error transporter sendmail :', err);
            return false;
          }
          console.log('Message sent infos :', info);
          resp = info.messageId;
        });
        
        return true;
      }

    }
    

    

    return true;
  } catch (err) {
    console.log(err);
    return false;
  }

};