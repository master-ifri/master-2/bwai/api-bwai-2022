// const Admin = require('../models/admin'); 
const User = require('../models/user');
//const History = require('../models/historiques');
const crypto = require('crypto');
const passport = require('passport');
const jwt = require('jsonwebtoken');

const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET_KEY
};

passport.use(new JwtStrategy(opts, async function(jwt_payload, done) {
  try {
    // const admin = await Admin.findOne({_id: jwt_payload.id});
    const user = await User.findOne({_id: jwt_payload.id});

    // let u = admin || user;
    let u = user;

    if (u) {  
      return done(null, u);
    } else {      
      return done('No user found!', false);
    }

  } catch (err) {
    return done(err, false);
  }
}));

const hashPassword = function (password) {
  var shaSum = crypto.createHash('sha256');
  shaSum.update(password);
  return shaSum.digest('hex');
}

const login = function (user, res) {
  jwt.sign({id: user._id}, process.env.JWT_SECRET_KEY, { expiresIn: 7 * 24 * 3600 }, (err, token) => {
    if (err) {
      console.log(err);
      return res.status(500).send(err);
    }

    user.password = '';
    // console.log("User infos ", user);


    if(user && user.statut == false) {
      
      res.json({
        status: 409,
        // token,
        user: user
      });

    } else {

      // const newHistory = new History({
      //   action: 'Connexion sur la plateforme',
      //   user: user._id
      // });
  
      // if(newHistory.save()){
      //   console.log("Un utilisateur vient de se connecter");
      // }

      res.json({
        status: 200,
        token,
        user: user
      });

    }

  });  
};

const verifyAndGenerateToken = function (err, user, res) {
  if (err) { 
    console.log(err);
    res.json({
      status: 500,
      message: err
     });
  }
  
  if (!user) {
   return res.json({
    status: 400,
    message: "Email or password incorrect !"
   });
  } else {
    login(user, res);
  }

};

const verifyLogin = function (Model, {email, password}, res) {
  const encPassword = hashPassword(password);
  Model.findOne({ email, password: encPassword }, function (err, user) {
    verifyAndGenerateToken(err, user, res);
  });
}

exports.login = login;
exports.hashPassword = hashPassword;

// exports.adminLogin = function (req, res, next) {
//   verifyLogin(Admin, req.body, res);
// };

exports.userLogin = function (req, res, next) {
  verifyLogin(User, req.body, res);
};

exports.verifyLoggedUser = passport.authenticate('jwt', { session: false });