let Article = require('../models/article');
let s3 = require('../services/s3');
const path = require('path');
//let History = require('../models/historiques');

exports.create = async function(req, res) {
  const email = req.body.email;  
  const format = req.body.format;  
  const photo = req.body.photo;
  const docname = req.body.docname;
  const link = req.body.link || [];

  try {
    let article = new Article({
      email,
      format,
      photo,
      link,
      docname
    });
    
    let savedArticle = await article.save();
    
    res.json({
      status: 200,
      message: 'Enregistrement réussi',
      // ressources: savedArticle
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.createWithFile = async function(req, res) {
  // console.log("Request ", req.protocol);

  const email = req.body.email;
  let docname = req.body.docname || '---';
  let format = req.body.format || '-';
  const link = req.body.link || '';

  try{

    let doc = '';

    if(req.file){
      console.log("File found ", req.file);

    } else {
      console.log("File not found");
    }

    if(req.file && req.file.location.length > 1){
      // console.log("File infos : ", req.file);
      doc = req.file.location;
      docname = req.file.originalname
      taille = req.file.originalname.split('.').length || 1;

      format = req.file.originalname.split('.')[taille-1]

      // console.log("Hay ",format, ' : ', req.file.originalname.split('.'))
    } else {
      doc = link && link.length > 10 ? link : ''
    }
    console.log("Doc info ", doc);

    // console.log("File info ", photo, ' || ', req.headers.host);
    
    // const photo = req.file ? req.file.location : '';
    // console.log("email : ", email, " Photo url :", req.file.location);

    const article = new Article({
      email,
      doc,
      format,
      docname,
      etat: 'unassign'
    });
    
    await article.save();

    // const newHistory = new History({
    //   action: "Enregistrement d'une actualité sur la plateforme",
    //   createdAt: lastData.createdAt,
    //   user: req.body.userId
    // });

    // await newHistory.save();

    res.json({
      status: 200,
      message: 'Enregistrement réussi',
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.getDetails = async function(req, res){
  const resourceId = req.params.id;

  try {
    let resources = await Article.findOne({_id: resourceId});
    
    res.json({
      status: 200,
      ressources: resources
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.list = async function(req, res) {
  try {
    // console.log("Query : ", req.query);

    let option = req.query.options ? JSON.parse(req.query.options) : {};
    let page = req.query.page ? parseInt(req.query.page) : 0;
    let sort = req.query.sort ? req.query.sort : {"createdAt": -1};
    let perPage = req.query.perPage ? parseInt(req.query.perPage) : 50;

    // console.log("Hi ", option, req.query.options.length);

    // let options = option;
    let options = {};
    if(req.query.filter && JSON.parse(req.query.filter).text && JSON.parse(req.query.filter).text.length>1){
      options = ({
        // ...option,
        $or:[
          {email: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {format: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {docname: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {doc: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
          {etat: {$regex: new RegExp(JSON.parse(req.query.filter).text, "i") }},
        ]
      })
    }

    const resources = await Article.find(options)
      // .populate(this.populateFields)
      .skip(page * perPage)
      .limit(perPage)
      .sort({"createdAt": -1})
      .lean();

    const database = await Article.find(options).countDocuments();

    res.json({
      status: 200,
      count: database,
      ressources: resources,
      total: resources.length
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.count = async function(req, res) {
  try {
    let query = {};
    let total = await Article.countDocuments();
    
    res.json({
      status: 200,
      ressource: total
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function(req, res) {
  const email = req.body.email;  
  const format = req.body.format; 
  const docname = req.body.docname; 
  const etat = req.body.etat; 
  // const link = JSON.parse(req.body.link);
   
  // const photo = req.body.photo;
  const resourceId = req.params.id;

  try {
    let doc = '';
    // const proto = req.protocol || req.headers.referer.split(':')[0];

    if(req.file && req.file.filename.length > 1){
      doc = req.file.filename;
    } else {
      doc = req.body.link;
    }

    // const photo = req.file ? req.file.location : req.body.image;

    const lastData = await Article.findByIdAndUpdate(resourceId, { email, format, doc, docname, etat });

    // const newHistory = new History({
    //   action: "Mises à jour d'une actualité sur la plateforme",
    //   createdAt: lastData.updatedAt,
    //   user: req.body.userId
    // });

    // await newHistory.save();

    res.json({
      status: 200,
      message: 'Ressource is up to date successfully'
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function(req, res) {
  const resourceId = req.params.id;
  try {
    await Article.findByIdAndDelete(resourceId);

    // const newHistory = new History({
    //   action: "Suppression d'une actualité sur la plateforme",
    //   user: req.body.userId
    // });

    // await newHistory.save();
    
    res.json({
      status: 200, 
      message: 'Resource supprimée' 
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};